/**
 * View Models used by Spring MVC REST controllers.
 */
package com.company.vinja.web.rest.vm;
